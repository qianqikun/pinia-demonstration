import vue from '@vitejs/plugin-vue'
import * as path from 'path'

const resolve = (p) => {
  return path.resolve(__dirname, p)
}

/**
 * @type {import('vite').UserConfig}
 */
export default {
  resolve: {
    alias: {
      '@': resolve('./src')
    }
  },
  plugins: [vue()],
  cssPreprocessOptions: {
    scss: {}
  }
}
